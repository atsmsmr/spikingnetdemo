//
//  ConstParams.h
//  SpikingNeuronsSimulator_Robot
//
//  Created by atsmsmr on 2015/06/21.
//
//

#ifndef SpikingNeuronsSimulator_Robot_ConstParams_h
#define SpikingNeuronsSimulator_Robot_ConstParams_h

#include <iostream>
#include <stdlib.h>

class ConstParams
{
public:
    static int Number_Of_Neurons;
    static int Number_Of_Inhibitory;
    static int Number_Of_Connection;
    static int Connection_Rate;
    static const double Weight_Max;
    static const double Weight_Min;
    static const int Network_Type;
    static const int Excitatory_Neuron_Type;
    static const int Inhibitory_Neuron_Type;
    
    static int Input_Neuron_Num;
    static int Input_Group_Num;
    static int Output_Neuron_Num;
    static int Output_Group_Num;
    

    
    static int Stim_Rate;
    static double Stim_Strength;
    static const double Decay_Rate;
    static int Random_Seed;
    static const int Resting_Time;
    static const int Max_Stim_Num;

    
    static const double Init_Weight_Ratio_Exc;
    static const double Init_Weight_Ratio_Inh;
    static double Noise_Ratio_Exc;
    static double Noise_Ratio_Inh;
    
    static const int Regular_Spiking_Demo = 1;
    static const int Resonator_Demo = 2;
    static const int Regular_Spiking = 3;
    static const int Resonator = 4;
    static const int Chattering = 5;
    
    static const int Random_Graph = 0;
    static const int Sparse_Net = 1;
    static const int NP_Graph = 2;
    
    static const int Experiment_Type;
    static const int Standard_LSA = 0;
    static const int Random_Stop_Stim = 1;
    static const int No_Stop_Stim = 2;

    static const std::string Load_FilePath;
    static const double Environment_Width;
    static const double Environment_Height;
    
    static const int Env_Id;
    
    static bool STP_Flag;
    static bool STDP_Flag;
    
    static const bool Is_Save_Spikes;
    static const bool Is_Save_Weights;
    
    static const std::string Save_Path;

    static const int Stabilizing_Time;
    

    virtual ~ConstParams(){}
};

#endif

