#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetBackgroundAuto(true);
    ofSetVerticalSync(false);
    ofBackground(0);
    
    spike_net.init();
    
    std::string save_dir = ConstParams::Save_Path;

    if(ConstParams::Is_Save_Spikes){
        writer_spike.open(save_dir + "spike_data_" + ofGetTimestampString() + ".txt");
    }
    
    if(ConstParams::Is_Save_Weights && ofGetFrameNum() % 10000 == 0){
        writer_weight.open(save_dir + "weight_data_" + ofGetTimestampString() + ".txt");
    }
    
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    spike_net.update();
    
    if(ofGetFrameNum()%100 == 0){
        spike_net.sendStim(0, 100); // (output group id, stimlation strength)
        spike_net.sendStim(1, 100); // (output group id, stimlation strength)
    }
    
    // log
    if(ConstParams::Is_Save_Spikes){
        writer_spike.writeSpikes(ofGetFrameNum(), spike_net.spiked_neuron_id);
    }
    
    if(ConstParams::Is_Save_Weights && ofGetFrameNum() % 1000 == 0){
        writer_weight.writeWeights(ofGetFrameNum(), spike_net.connection);
    }

}

//--------------------------------------------------------------
void ofApp::drawNeuron(){
    
    int rectsize = 10;
    int n2 = sqrt(ConstParams::Number_Of_Neurons);
    
    for(int i=0; i<ConstParams::Number_Of_Neurons; i++){
        if(spike_net.neurons[i].isFiring()){
            if(spike_net.neurons[i].getNeuronType() == ConstParams::Regular_Spiking_Demo || spike_net.neurons[i].getNeuronType() == ConstParams::Regular_Spiking){
                ofSetColor(255,0,0);
            }else{
                ofSetColor(0,255,0);
            }
        }
        else {
            ofSetColor(-spike_net.neurons[i].getV());
        }
        ofRect(rectsize*(i%n2)+20,rectsize*int(i/n2)+20,rectsize,rectsize);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    drawNeuron();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}
