#pragma once

#include <iomanip>
#include <fstream>
#include <time.h>
#include <sys/stat.h>
#include <math.h>
#include <string>
#include <cmath>

#include "ofMain.h"

#include "Parameters.h"
#include "SpikingNet.h"
#include "SpikeNetWriter.h"

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    
private:
    void drawNeuron();
    void setFrameRate(int & fps);
    void setStpFlag(bool & stp_flag);
    void setStdpFlag(bool & stdp_flag);
    void resetNN(){spike_net.init();}
    void setStimRatio(int & stim_ratio){ConstParams::Stim_Rate = stim_ratio;}
    void setStimStrength(float & _stim_strength){ConstParams::Stim_Strength = _stim_strength;}
    void setNoiseExc(float & _noise_exc){ConstParams::Noise_Ratio_Exc = _noise_exc;}
    void setNoiseInh(float & _noise_inh){ConstParams::Noise_Ratio_Inh = _noise_inh;}
    
    SpikingNet spike_net;
    SpikeNetWriter writer_weight, writer_spike;
};
