//
//  neuronCtrl.h
//  SpikingNeuronSimulator
//
//  Created by atsmsmr on 2014/10/20.
//  Copyright (c) 2014年 Atsushi Masumori. All rights reserved.
//
#pragma once

#include <fstream>
#include <time.h>
#include <sys/stat.h>
#include <cmath>
#include <sstream>
#include <stdlib.h>

#include <random>
#include <algorithm>
#include <memory>
#include <vector>

#include "Izhikevich.h"
#include "Synapse.h"
#include "Parameters.h"

class SpikingNet : private ConstParams{
    
private:
    void update_neuron();
    void update_input();
    void checkFiring();
    void stdp();
    void stp();
    double getStpValue(int index, int is_fired);
    void clearSpikedNeuronId();
    void decayWeight();
    void setRandomNetwork();
    void setSparseNetwork();
    void setInputNeurons();
    void setOutputNeurons();
    int* getPartOutputNeuron(int start_index, int size);
    bool any(int target, const int *reference, int sizeOfReference);
    double rand_normal();
    double rand_normal( double mu, double sigma );

    std::vector<int> input_neurons;
    std::vector<int> output_neurons;
    std::vector<int> spiked_neuron_id_cum;
    std::vector<int> spiked_num_of_output_group;
    std::vector<int> excitatory_connections_list;
    std::vector<bool> stim_flags;
    std::vector<float> stim_strengths;

    // STDP
    std::vector<int> stdp_counts;
    std::vector<int> stdp_spiked_time;
    int stdp_tau;
    
    // STP (Science paper)
    double* stp_u;
    double* stp_x;
    double* stp_wf;
    
    int frame_count;
    std::mt19937 rand_gen;
    
public:
    SpikingNet();
    ~SpikingNet();
    
    void init();
    void update();
    void checkTask();
    void sendStim(int output_group_id, double strength);
    void sendStimAll(double stim_strength);
    int getFrameCount(){return frame_count;};
    double* getBurstinessIndex();
    vector<int> getSpikedNumList();
    std::vector<int> getSpikedIds(){return spiked_neuron_id;};
    std::vector<Synapse> getConnection(){return connection;};

    std::vector<Izhikevich> neurons;
    std::vector<Synapse> connection;
    std::vector<int> spiked_neuron_id;
};