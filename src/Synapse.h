//
//  Synapse.h
//  spiking_neurons_simulator
//
//  Created by atsmsmr on 2016/05/25.
//  Copyright (c) 2016年 Atsushi Masumori. All rights reserved.
//

#ifndef __spiking_neurons_simulator__Synapse__
#define __spiking_neurons_simulator__Synapse__

#include <stdio.h>

class Synapse
{
public:
    Synapse(){};
    Synapse(int src_, int dest_, double weight_, int type_){
        this->weight = weight_;
        this->src = src_;
        this->dest = dest_;
        this->type = type_;
    };
    ~Synapse(){};
    double weight;
    int src;
    int dest;
    int type;
    
private:
    
};




#endif /* defined(__spiking_neurons_simulator__Synapse__) */
