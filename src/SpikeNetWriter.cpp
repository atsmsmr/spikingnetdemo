//
//  SpikeNetWriter.cpp
//  spiking_neurons_simulator
//
//  Created by atsmsmr on 2016/05/30.
//  Copyright (c) 2016年 Atsushi Masumori. All rights reserved.
//

#include "SpikeNetWriter.h"

SpikeNetWriter::SpikeNetWriter(){

}

SpikeNetWriter::~SpikeNetWriter(){
    this->close();
}

void SpikeNetWriter::open(std::string filepath){
    sn_stream.open(filepath);
};

void SpikeNetWriter::open(char* filepath){
    sn_stream.open(filepath);
};

void SpikeNetWriter::write(double value){
        sn_stream << value << std::endl;
};

void SpikeNetWriter::writeWeights(int current_frame, std::vector<Synapse> &connection){
    
    
    for(int i=0; i<connection.size(); ++i){
        std::vector<float> line;
        line.push_back(current_frame);
        line.push_back(connection[i].type);
        line.push_back(connection[i].src);
        line.push_back(connection[i].dest);
        line.push_back(connection[i].weight);
        this->writeSome(line);
    }
    
};

void SpikeNetWriter::writeSpikes(int current_frame, std::vector<int> &spiked_neuron_ids){
    if(spiked_neuron_ids.size()>0){
    sn_stream << current_frame << ",";
    for(int i=0; i<spiked_neuron_ids.size(); i++){
        sn_stream << spiked_neuron_ids[i];
        if(i == spiked_neuron_ids.size()-1) sn_stream << "\n";
        else sn_stream << ",";
    }
    }
    
};

void SpikeNetWriter::writeSome(std::vector<float> some_values){
    
    for(int i=0; i<some_values.size(); i++){
        sn_stream << some_values[i];
        if(i != some_values.size()-1) sn_stream << ",";
        else sn_stream << "\n";
    }
    if(some_values.size()==0) sn_stream << "\n";
};


void SpikeNetWriter::close(){
    sn_stream.close();
}