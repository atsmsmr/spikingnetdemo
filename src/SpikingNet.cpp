//
//  neuronCtrl.cpp
//  SpikingNeuronSimulator
//
//  Created by atsmsmr on 2014/10/20.
//  Copyright (c) 2014年 Atsushi Masumori. All rights reserved.
//

#include "SpikingNet.h"
SpikingNet::SpikingNet(){
    
    stdp_tau = 20;
    
    //set random seed
    rand_gen.seed(ConstParams::Random_Seed);
    
    neurons.resize(ConstParams::Number_Of_Neurons);
    input_neurons.resize(ConstParams::Input_Neuron_Num, 0);
    output_neurons.resize(ConstParams::Output_Neuron_Num, 0);
    spiked_num_of_output_group.resize(ConstParams::Output_Group_Num, 0);
    
    stdp_spiked_time.resize(ConstParams::Number_Of_Neurons, 0);
    stdp_counts.resize(ConstParams::Number_Of_Neurons, 0);
    
    stim_flags.resize(ConstParams::Input_Group_Num);
    stim_strengths.resize(ConstParams::Input_Group_Num);
    
    //STP (Science paper)
    stp_u = new double[Number_Of_Neurons];
    stp_x = new double[Number_Of_Neurons];
    stp_wf = new double[Number_Of_Neurons];
}

SpikingNet::~SpikingNet(){
    
    // delete STP variables
    delete[] stp_u;
    delete[] stp_x;
    delete[] stp_wf;
}


void SpikingNet::init(){
    
    connection.clear();
    excitatory_connections_list.clear();
    neurons.clear();
    input_neurons.clear();
    output_neurons.clear();
    spiked_num_of_output_group.clear();
    stdp_spiked_time.clear();
    stdp_counts.clear();
    
    neurons.resize(ConstParams::Number_Of_Neurons);
    input_neurons.resize(ConstParams::Input_Neuron_Num, 0);
    output_neurons.resize(ConstParams::Output_Neuron_Num, 0);
    spiked_num_of_output_group.resize(ConstParams::Output_Group_Num, 0);

    stdp_spiked_time.resize(ConstParams::Number_Of_Neurons, 0);
    stdp_counts.resize(ConstParams::Number_Of_Neurons, 0);
    
    //initialization of some variables
    frame_count = 0;
    
    for(int i=0; i<Number_Of_Neurons; ++i){
        
        stdp_spiked_time[i] = 0;
        stdp_counts[i] = 0;
        
        // initialize STP variables
        stp_wf[i] = 1.0;
        stp_x[i] = 1.0;
        stp_u[i] = 0.0;
    }
    
    for(int i=0; i<Number_Of_Neurons; ++i){
        if(i < Number_Of_Inhibitory){
            neurons[i].setNeuronType(Inhibitory_Neuron_Type);
        }else{
            neurons[i].setNeuronType(Excitatory_Neuron_Type);
        }
    }
    
    switch(Network_Type){
            
        case Sparse_Net:
            setSparseNetwork();
            break;
            
        case Random_Graph:
            setRandomNetwork();
            break;
    }
    setInputNeurons();
    setOutputNeurons();
}

void SpikingNet::setInputNeurons(){
    for(int i=0; i<Input_Neuron_Num; ++i){
        input_neurons[i] = Number_Of_Inhibitory + i;
    }
}

void SpikingNet::setOutputNeurons(){
    for(int i=0; i<Output_Neuron_Num; ++i){
        output_neurons[i] = Number_Of_Inhibitory + Input_Neuron_Num + i;
    }
}

void SpikingNet::setSparseNetwork(){
    
    std::uniform_int_distribution<> rand_uni_100(0, 99);
    std::uniform_real_distribution<> rand_uni(0.0, 1.0);
    connection.clear();
    
    //    std::cout << rand_uni_100(rand_gen) << std::endl;
    //    std::cout << rand_uni(rand_gen) << std::endl;
    
    
    for(int i=0; i<Number_Of_Neurons; ++i){
        for(int j=0; j<Number_Of_Neurons; j++){
            if(i!=j && rand_uni_100(rand_gen) < Connection_Rate){
                if(neurons[i].getNeuronType() == Excitatory_Neuron_Type && neurons[j].getNeuronType() == Excitatory_Neuron_Type){
                    connection.push_back( *new Synapse(i, j, Init_Weight_Ratio_Exc*rand_uni(rand_gen), 0));
                    excitatory_connections_list.push_back(int(connection.size()));
                    
                }else if(neurons[i].getNeuronType() == Excitatory_Neuron_Type && neurons[j].getNeuronType() == Inhibitory_Neuron_Type){
                    connection.push_back( *new Synapse(i, j, Init_Weight_Ratio_Exc*rand_uni(rand_gen), 1));
                    
                }else if(neurons[i].getNeuronType() == Inhibitory_Neuron_Type){
                    connection.push_back( *new Synapse(i, j, Init_Weight_Ratio_Inh*rand_uni(rand_gen), 1));
                    
                }
            }
        }
    }
}

// fully connected network with random weight
void SpikingNet::setRandomNetwork(){

}

int* SpikingNet::getPartOutputNeuron(int start_index, int size){
    int* part_output = new int[size];
    for(int i=0; i<size; ++i){
        part_output[i] = output_neurons[i+start_index];
    }
    return part_output;
}

void SpikingNet::checkFiring(){
    spiked_neuron_id.clear();
    for(int i=0; i<Number_Of_Neurons; ++i){
        if(neurons[i].checkFiring()) {
            spiked_neuron_id.push_back(i);
            spiked_neuron_id_cum.push_back(i);
            neurons[i].setSpikedTime(frame_count);
        }
    }
}

void SpikingNet::checkTask(){
    
    // Remove overlaps
    std::sort(spiked_neuron_id_cum.begin(), spiked_neuron_id_cum.end());
    spiked_neuron_id_cum.erase(std::unique(spiked_neuron_id_cum.begin(), spiked_neuron_id_cum.end()), spiked_neuron_id_cum.end() );
    
    int group_size = floor((float)ConstParams::Output_Neuron_Num / (float)ConstParams::Output_Group_Num);
    for(int i=0; i<spiked_neuron_id_cum.size(); i++){
        
        for(int j=0; j<ConstParams::Output_Group_Num; j++){
            
            //        std::cout << spikedNeuronID[i] << std::endl;
            int exclude_size = ConstParams::Number_Of_Inhibitory + ConstParams::Input_Neuron_Num;
            int* output_group = getPartOutputNeuron(j*group_size, group_size);
            
            if(any(spiked_neuron_id_cum[i], output_group, group_size)){
                spiked_num_of_output_group[j] += 1;
            }
            delete output_group;
        }
    }
}


void SpikingNet::clearSpikedNeuronId(){
    spiked_num_of_output_group.clear();
    spiked_num_of_output_group.resize(ConstParams::Output_Group_Num, 0);
    spiked_neuron_id_cum.clear();
}


vector<int> SpikingNet::getSpikedNumList(){
    return spiked_neuron_id;
}

void SpikingNet::update_input(){
    
    //setup normal distribution random function
    std::normal_distribution<> normalRand(0.0, 1.0);
    
    //pseudo thalamus noise-input (Izhikevich paper)
    for(int i=0; i<Number_Of_Neurons; ++i){
        
        if(i < Number_Of_Inhibitory ){
            neurons[i].addToI(Noise_Ratio_Inh*normalRand(rand_gen));
        }else{
            neurons[i].addToI(Noise_Ratio_Exc*normalRand(rand_gen));
        }
    }
    
    // External input
    int group_size = floor((float)ConstParams::Input_Neuron_Num / (float)ConstParams::Input_Group_Num);
    for(int i=0; i<Input_Group_Num; i++){
        if(stim_flags[i]){
            for(int j=0; j<group_size; j++){
                neurons[input_neurons[i*group_size+j]].addToI(stim_strengths[i]);
            }
            stim_flags[i] = false;
        }
    }
    
    // Input from pre-synaptic neurons with STP
    int src, dest;
    for(int i=0; i<connection.size(); ++i){
        
        src = connection[i].src;
        dest = connection[i].dest;
        
        if(neurons[src].firing == true){
            if(ConstParams::STP_Flag){
                if(connection[i].type == 0){// only for exc-exc connection
                    neurons[dest].addToI(connection[i].weight * stp_wf[src]);
                }else{
                    neurons[dest].addToI(connection[i].weight);
                }
            }else{
                neurons[dest].addToI((connection[i].weight));
            }
        }
    }
    
}

void SpikingNet::update_neuron(){
    //update differential equation
    for(int i=0; i<Number_Of_Neurons; ++i){
        neurons[i].update();
        neurons[i].setI(0.0);
    }
}

void SpikingNet::stdp(){
    for(int i=Number_Of_Inhibitory; i<Number_Of_Neurons; ++i){
        if(stdp_counts[i]>0) stdp_counts[i] = stdp_counts[i] - 1;
        if(neurons[i].firing) stdp_counts[i] = stdp_tau;
    }
    
    int index, src, dest;
    double d;
    int stdp_count;
    for(int i=0; i<excitatory_connections_list.size(); ++i){// only excitatory synapse
        
        index = excitatory_connections_list[i];
        src = connection[index].src;
        dest = connection[index].dest;
        
        if(connection[index].weight > 0){
            if(neurons[src].firing){
                stdp_count = stdp_counts[dest];
                if(0 < stdp_count && stdp_count < stdp_tau){
                    d = 0.1 * pow(0.95, (stdp_tau-stdp_count));
                    connection[index].weight -= d;
                    if     (connection[index].weight >  Weight_Max) connection[index].weight = Weight_Max;
                    else if(connection[index].weight <= Weight_Min) connection[index].weight = Weight_Min;
                }
                
            }else if(neurons[dest].firing){
                
                stdp_count = stdp_counts[src];
                if( 0 < stdp_count && stdp_count < stdp_tau){
                    d = 0.1*pow(0.95, (stdp_tau-stdp_count));
                    connection[index].weight += d;
                    if     (connection[index].weight >  Weight_Max) connection[index].weight = Weight_Max;
                    else if(connection[index].weight <= Weight_Min) connection[index].weight = Weight_Min;
                }
            }
        }
    }
}

void SpikingNet::stp(){
    
    for(int i=Number_Of_Inhibitory; i<Number_Of_Neurons; ++i){
        
        if(neurons[i].firing){
            stp_wf[i] = getStpValue(i,1);
            if(stp_wf[i] > 1.0) stp_wf[i] = 1.0;
        }else{
            stp_wf[i] = getStpValue(i,0);
            if(stp_wf[i] > 1.0) stp_wf[i] = 1.0;
        }
    }
}

double SpikingNet::getStpValue(int index, int is_firing){
    double wf = 0.;
    
    double s = double(is_firing);
    double u = stp_u[index];
    double x = stp_x[index];
    
    double tau_d = 200.;//ms
    double tau_f = 600.;//ms
    double U = 0.2;//mV
    
    double dx = (1.0-x)/tau_d - u*x*s;
    double du = (U - u)/tau_f + U*(1.0-u)*s;
    
    double nu = u+du;
    double nx = x+dx;
    
    wf = nu*nx;
    stp_u[index] = nu;
    stp_x[index] = nx;
    return wf;
}

void SpikingNet::decayWeight(){
    int connection_size = int(connection.size());
    for(int i=0; i<connection_size; ++i){
        if(connection[i].type == 0){// only for exc-exc connection
            connection[i].weight *= ConstParams::Decay_Rate;
        }
    }
}

void SpikingNet::update(){
    checkFiring();
    if(ConstParams::STDP_Flag) stdp();
    if(ConstParams::STP_Flag) stp();
    update_input();
    update_neuron();
    decayWeight();
    frame_count++;
}

//void SpikingNet::saveParams(){
//    std::ofstream ofs_param;
//    std::stringstream frameCount_;
//    frameCount_ << frameCount;
//    std::string param_path = data_path.str() + "parameterSet_" + frameCount_.str() + ".csv";
//    ofs_param.open(param_path.c_str());
//    
//    double a,b,c,d,I,u,V,type;
//    for(int i=0; i<Number_Of_Neurons; ++i){
//        a = neurons[i].getA();
//        b = neurons[i].getB();
//        c = neurons[i].getC();
//        d = neurons[i].getD();
//        I = neurons[i].getI();
//        V = neurons[i].getV();
//        u = neurons[i].getU();
//        type = neurons[i].getNeuronType();
//        ofs_param << i << "," << type << "," << a << "," << b << "," << c << "," << d << "," << u << "," << I << "," << V << "\n";
//    }
//}

//void neuronCtrl::saveNetwork(){
//    std::ofstream ofs_network;
//    std::stringstream frameCount_;
//    frameCount_ << frameCount;
//    std::string networkdata_path = data_path.str() + "synapticWeight_" + frameCount_.str() +  ".csv";
//    ofs_network.open(networkdata_path.c_str());
//
//    for(int i=0; i<Number_Of_Neurons; ++i){
//        for(int j=0; j<Number_Of_Neurons; j++){
//            ofs_network << i << "," << j << "," << synapticWeight[i][j];
//            ofs_network << "\n";
//        }
//    }
//    ofs_network << std::endl;
//    ofs_network.close();
//    //    std::cout << "Finished to save network data" << std::endl;
//}

//void SpikingNet::setIZParams(string path){
//    
//    ifstream csvdata(path.c_str());
//    if(csvdata.fail()){
//        cout << "user error: No such file." << endl;
//    }
//    
//    string str;
//    int n = 0;
//    while(getline(csvdata,str))
//    {
//        double a,b,c,d,u,I,v;
//        int id, type;
//        sscanf(str.c_str(),"%d,%d,%lf,%lf,%lf,%lf,%lf,%lf,%lf",&id,&type,&a,&b,&c,&d,&u,&I,&v);
//        neurons[id].setParam(type,a,b,c,d,u,I,v);
//    }
//}

void SpikingNet::sendStim(int output_group_id, double strength){
    
    if(output_group_id >= ConstParams::Output_Group_Num){
        // error message is required here.
    }else{
        stim_flags[output_group_id] = true;
        stim_strengths[output_group_id] = strength;
    }
    
}

void SpikingNet::sendStimAll(double strength){
    for(int i=0; i<stim_flags.size(); ++i){
        stim_flags[i] = true;
        stim_strengths[i] = strength;
    }
}

// Get level of bursting according to "Contorolling Bursting in Cortical Cultures with Closed-Loop Multi-Electeode Stimulation"
double* SpikingNet::getBurstinessIndex(){
    
    const int Recording_Frames = 5000; // 5 min
    const int Stabilizing_Frames = 5000; // 1min
    const int Bin_Width = Recording_Frames/100; // 1s
    int bin_spikes = 0;
    int sum_spikes = 0;
    std::vector<int> bin_spikes_list;
    bin_spikes_list.clear();
    
    double firing_rate = 0.0;
    int num_spikes[Number_Of_Neurons];
    
    for(int i=0; i<Number_Of_Neurons; ++i){
        num_spikes[i] = 0;
    }
    this->init();
    
    // Stabilizing a network (Pre-processing for 1 min)
    for(int i=0; i<Stabilizing_Frames; ++i){
        if(i%int(Stim_Rate) == 0) this->sendStimAll(ConstParams::Stim_Strength);
        this->update();
    }
    
    // Recording spikes
    for(int i=0; i<Stabilizing_Frames; ++i){
        if(i%int(Stim_Rate) == 0) this->sendStimAll(ConstParams::Stim_Strength);
        this->update();
        bin_spikes += this->spiked_neuron_id.size();
        //        std::cout << this->spikedNeuronID.size() << std::endl;
        if(i%Bin_Width == 0){
            sum_spikes += bin_spikes;
            bin_spikes_list.push_back(bin_spikes);
            bin_spikes = 0;
        }
        
        // records spikes for firing rate.
        for(int j=0; j<this->spiked_neuron_id.size(); j++){
            int index = spiked_neuron_id[j];
            num_spikes[index] += 1;
        }
    }
    
    // Get sum of top 15 bin_spikes
    std::sort(bin_spikes_list.begin(),bin_spikes_list.end(),std::greater<int>());
    double top15_bin_spikes = 0.0;
    
    for(int i=0; i<int((Recording_Frames/Bin_Width)*0.15); ++i){
        top15_bin_spikes += bin_spikes_list[i];
    }
    // calc burstiness index
    double burstiness_index = (top15_bin_spikes/sum_spikes);
    
    // calc firing rate
    double sum = 0;
    
    for(int i=0; i<Number_Of_Neurons; ++i){
        sum += num_spikes[i];
    }
    firing_rate = sum/(Recording_Frames*Number_Of_Neurons);
    
    //        printf("firing rate: %f\n", firing_rate);
    //        printf("burstiness index: %f\n", burstiness_index);
    std::cout << Number_Of_Neurons << "," << firing_rate << "," << burstiness_index << std::endl;
    
    double* result = new double[2];
    result[0] = firing_rate;
    result[1] = burstiness_index;
    
    return result;
}

bool SpikingNet::any(int target, const int *reference, int reference_size){
    bool answer = false;
    for(int i=0; i<reference_size; ++i){
        if(target == reference[i]){
            answer = true;
            break;
        }
    }
    return answer;
}
