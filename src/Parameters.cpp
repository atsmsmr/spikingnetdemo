//
//  File.cpp
//  SpikingNeuronsSimulator_Robot
//
//  Created by atsmsmr on 2015/06/21.
//
//

#include "Parameters.h"

int ConstParams::Number_Of_Neurons = 500;
int ConstParams::Number_Of_Inhibitory = Number_Of_Neurons/5;
int ConstParams::Connection_Rate = 100 * 100/Number_Of_Neurons;
//int ConstParams::Number_Of_Connection = 10;

const int ConstParams::Network_Type = ConstParams::Sparse_Net;
const int ConstParams::Excitatory_Neuron_Type = ConstParams::Regular_Spiking;
const int ConstParams::Inhibitory_Neuron_Type = ConstParams::Resonator;

int ConstParams::Input_Neuron_Num = ConstParams::Number_Of_Neurons/5;
int ConstParams::Output_Neuron_Num = ConstParams::Number_Of_Neurons/5;
int ConstParams::Output_Group_Num = 2;
int ConstParams::Input_Group_Num = 2;

// for neuron
const double ConstParams::Init_Weight_Ratio_Exc = 5.0;
const double ConstParams::Init_Weight_Ratio_Inh = -5.0;
double ConstParams::Noise_Ratio_Exc = 5.0;
double ConstParams::Noise_Ratio_Inh = 3.0;
const double ConstParams::Weight_Max = 20.0;
const double ConstParams::Weight_Min = 0.0;
int ConstParams::Stim_Rate = 10; // for getBurstinessIndex()
double ConstParams::Stim_Strength = 100; // for getBurstinessIndex()

bool ConstParams::STP_Flag = true;
bool ConstParams::STDP_Flag = true;
const bool ConstParams::Is_Save_Spikes  = false;
const bool ConstParams::Is_Save_Weights = false;
const std::string ConstParams::Save_Path = "/save/path/";

const double ConstParams::Decay_Rate = 0.9999995;
int ConstParams::Random_Seed = 123456283;
