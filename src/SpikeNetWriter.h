//
//  SpikeNetWriter.h
//  spiking_neurons_simulator
//
//  Created by atsmsmr on 2016/05/30.
//  Copyright (c) 2016年 Atsushi Masumori. All rights reserved.
//
#pragma once
#ifndef __spiking_neurons_simulator__SpikeNetWriter__
#define __spiking_neurons_simulator__SpikeNetWriter__

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <sys/stat.h>
#include "Synapse.h"

class SpikeNetWriter{
private:
    
public:
    SpikeNetWriter();
    ~SpikeNetWriter();
    void open(std::string filepath);
    void open(char* filepath);
    void write(double value);
    void writeSpikes(int frame_current, std::vector<int> &spiked_neuron_ids);
    void writeWeights(int current_frame, std::vector<Synapse> &connection);
    void writeSome(std::vector<float> values);
    void writeString(std::string time);

    void close();
    
    std::ofstream sn_stream;
};
#endif /* defined(__spiking_neurons_simulator__SpikeNetWriter__) */
